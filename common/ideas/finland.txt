ideas = {

	country = {
		fin_sisu = {
			
			allowed = {
				always = no
			}
			
			picture = FIN_Lotta_Svard

			allowed_civil_war = {
				NOT = {
					has_government = communism
				}
			}

			removal_cost = -1
			
			modifier = {
				army_core_attack_factor = 0.75
				army_core_defence_factor = 0.75
				conscription = 0.04
				mobilization_speed = 0.05
				winter_attrition_factor = -0.1
				equipment_capture_factor = 0.1

			}
		}

		fin_slumping_economy = {
		
			allowed = {
				always = no
			}

			allowed_civil_war = {
				NOT = {
					has_government = communism
				}
			}

			removal_cost = -1
			
			picture = GER_Slumping_Economy
			
			modifier = {
			production_factory_max_efficiency_factor = -0.1
			production_factory_efficiency_gain_factor = -0.1
			industrial_capacity_factory = -0.1
			}
		}

		fin_rytis_economic_plan = {
		
			allowed = {
				always = no
			}

			allowed_civil_war = {
				NOT = {
					has_government = communism
				}
			}

			removal_cost = -1
			
			picture = chi_hyper_inflation_none
			
			modifier = {
			production_factory_efficiency_gain_factor = 0.05
			consumer_goods_factor = -0.05
			industrial_capacity_factory = 0.1
			}
		}

		fin_corrupt_cabinet = {
		
			allowed = {
				always = no
			}

			allowed_civil_war = {
				NOT = {
					has_government = communism
				}
			}

			removal_cost = -1
			
			picture = GENERIC_Corruption
			
			modifier = {
				consumer_goods_factor = 0.2
				political_power_cost = 0.15
				political_advisor_cost_factor = 0.2
				stability_factor = -0.1
			}
		}

		fin_finnish_intelligence_idea = {
		
			allowed = {
				always = no
			}

			removal_cost = -1
			
			picture = ROM_Lurking_Opposition
			
			modifier = {
				agency_upgrade_time = -0.25
				intel_network_gain_factor = 0.25
			}
		}

		fin_anti_soviet_struggle = {
		
			allowed = {
				always = no
			}

			removal_cost = -1
			
			picture = GER_Festung_Europa
			targeted_modifier = {
				tag = USA
				attack_bonus_against = -0.3
				defense_bonus_against = -0.3
			}
			targeted_modifier = {
				tag = ENG
				attack_bonus_against = -0.3
				defense_bonus_against = -0.3
			}
			targeted_modifier = {
				tag = CAN
				attack_bonus_against = -0.3
				defense_bonus_against = -0.3
			}
			targeted_modifier = {
				tag = SAF
				attack_bonus_against = -0.3
				defense_bonus_against = -0.3
			}
			targeted_modifier = {
				tag = AST
				attack_bonus_against = -0.3
				defense_bonus_against = -0.3
			}
			targeted_modifier = {
				tag = NZL
				attack_bonus_against = -0.3
				defense_bonus_against = -0.3
			}
		}
		
		
	}

	political_advisor = {

		generic_fascist_demagogue = {
			
			allowed = {
				original_tag = FIN
			}
			
			available = {
				if = {
					limit = { has_dlc = "Man the Guns" }	
					NOT = { has_autonomy_state = autonomy_supervised_state }
				}				
			}
			
			traits = { fascist_demagogue }
			do_effect = {
				NOT = {
					has_government = fascism
				}
			}
		}
		FIN_ralf_torngren = {

			picture = generic_political_advisor_europe_1

			allowed = {
				original_tag = FIN
			}
			
			traits = { ideological_crusader }
		}

		FIN_ake_henrik_gartz = {

			picture = generic_political_advisor_europe_2
				
			allowed = {
				original_tag = FIN
			}
			
			traits = { captain_of_industry }
		}

		FIN_antti_hackzell = {

			picture = generic_political_advisor_europe_3
			
			allowed = {
				original_tag = FIN
			}
			
			traits = { silent_workhorse }
		}
	}
	theorist = {

		FIN_akseli_anttila = {
			ledger = army
			
			picture = generic_army_europe_2		
					
			allowed = {
				original_tag = FIN
			}

			
			research_bonus = {
				land_doctrine = 0.10
			}
			
			traits = { military_theorist }
		}

		FIN_vaino_valve = {
			ledger = navy
					
			picture = generic_navy_europe_2

			allowed = {
				original_tag = FIN
			}
			

			
			research_bonus = {
				naval_doctrine = 0.10
			}
			
			traits = { naval_theorist }
		}
	}
	# MILITARY
	army_chief = {
		

		FIN_lauri_malmberg = {
			
			picture = generic_army_europe_2
			
			allowed = {
				original_tag = FIN
			}
			

			
			traits = { army_chief_drill_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		FIN_hugo_osterman = {
			
			picture = generic_army_europe_2
			
			allowed = {
				original_tag = FIN
			}
			

			
			traits = { army_chief_offensive_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		FIN_carl_mannerheim = {
			
			picture = carl_mannerheim
			
			allowed = {
				original_tag = FIN
			
			}
			
			available = { 
				has_completed_focus = fin_mannerheims_government
			}

			
			traits = { army_chief_defensive_3 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	air_chief = {
				

		FIN_jarl_lundqvist = {
			
			picture = generic_air_europe_3
			
			allowed = {
				original_tag = FIN
			}
			

			
			traits = { air_chief_all_weather_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		FIN_olavi_sarko = {
			
			picture = generic_air_europe_1
			
			allowed = {
				original_tag = FIN
			}
			

			
			traits = { air_chief_ground_support_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	navy_chief = {

		FIN_olavi_arho = {
			
			picture = generic_navy_europe_2
			
			allowed = {
				original_tag = FIN
			}
			

			
			traits = { navy_chief_commerce_raiding_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		FIN_eero_rahola = {
			
			picture = generic_navy_europe_1
			
			allowed = {
				original_tag = FIN
			}
			

			
			traits = { navy_chief_decisive_battle_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}

	high_command = {
		FIN_karl_oesch = {
			ledger = army
			
			
			allowed = {
				original_tag = FIN
			}
			
			picture = generic_army_europe_6
			
			traits = { army_regrouping_3 }
			
			ai_will_do = {
				factor = 1
			}
		}
		
		kenneth_anderson_2 = {
			ledger = army
			
			allowed = {
				original_tag = FIN
			}
			
			picture = generic_air_europe_2
			
			traits = { army_infantry_1 }
			
			ai_will_do = {
				factor = 2
			}
		}

		FIN_valio_porvari = {
			ledger = air
			
			
			allowed = {
				original_tag = FIN
			}
			
			picture = generic_army_europe_5
			
			traits = { air_air_superiority_2 }
			
			ai_will_do = {
				factor = 1
			}
		}

		claude_auchinleck_1 = {
			ledger = army
			
			allowed = {
				original_tag = FIN
			}
			
			available = {
				has_completed_focus = fin_mannerheims_government
			}
			
			picture = generic_army_europe_4
			
			traits = { army_armored_2 }
			
			ai_will_do = {
				factor = 2
			}
		}

		claude_auchinleck_2 = {
			ledger = army
			
			allowed = {
				original_tag = FIN
			}

			available = {
				has_completed_focus = fin_rytis_government
			}

			picture = generic_air_europe_1
			traits = { army_armored_1 }
			
			ai_will_do = {
				factor = 2
			}
		}

		FIN_lauri_tiainen_2 = {
			ledger = army
			
			
			allowed = {
				original_tag = FIN
			}
			
			picture = generic_army_europe_3
			

			available = {
				has_completed_focus = fin_mannerheims_government
			}
			traits = { army_commando_1 }
			
			ai_will_do = {
				factor = 1
			}
		}

		FIN_lauri_tiainen = {
			ledger = army
			
			
			allowed = {
				original_tag = FIN
			}
			
			picture = generic_army_europe_1
			
			traits = { army_commando_1 }

			available = {
				has_completed_focus = fin_rytis_government
			}
			
			ai_will_do = {
				factor = 1
			}
		}

		FIN_harald_ohquist = {
			ledger = army
			
			
			allowed = {
				original_tag = FIN
			}
			
			picture = generic_army_europe_5
			
			traits = { army_regrouping_2 }
			
			ai_will_do = {
				factor = 1
			}
		}
	}	

	# TECHNOLOGY

	naval_manufacturer = { 
		
		designer = yes
		
		crichton = {
			
			picture = generic_naval_manufacturer_1

			
			allowed = {
				original_tag = FIN
			}
			cost = 100
			research_bonus = {
				naval_equipment = 0.15
			}
			
			# submarine and defence ship production
			
			traits = { naval_manufacturer }
		}
	}
	
	aircraft_manufacturer = { 
			
		designer = yes
		
		
		valtion_lentokonetehdas = {
			
			picture = generic_air_manufacturer_3

			
			allowed = {
				original_tag = FIN
			}
			
			research_bonus = {
				air_equipment = 0.15
			}
			
			traits = { light_aircraft_manufacturer }
			
			# assembled Junkers, fast bombers
		
			ai_will_do = {
				factor = 1
			}
		}
	}
}