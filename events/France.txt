﻿###########################
# French Events
###########################

add_namespace = france

# Japan demands Indochina
country_event = {
	id = france.1
	title = france.1.t
	desc = france.1.d
	picture = GFX_report_event_japan_army_mountainside
	
	is_triggered_only = yes
	
	option = { # Accept demands
		name = france.1.a
		JAP = {
			country_event = { days = 5 id = japan.6 }
		}
	}
}




# France wants Armistice (Germany)
country_event = {
	id = france.14
	title = france.14.t
	desc = france.14.d
	picture = GFX_report_event_german_parade_paris

	is_triggered_only = yes
	fire_only_once = yes

	option = { # Agree
		name = france.14.a
		set_global_flag = achievement_france_surrender
		set_global_flag = fall_of_france
		add_political_power = 150
		if={limit={date<1940.2.1}add_timed_idea={idea=GER_triumph_in_france days=730}}
		FRA = {
			add_ideas = FRA_defeatism
			country_event = france.20
		}
		FRA = {
		set_equipment_fraction = 0
		}

		EFR = {
			transfer_state = 26
			every_state = {
				limit = { is_core_of = FRA }
				add_core_of = EFR
			}
			set_capital = 26
			set_politics = {
				ruling_party = fascism
				elections_allowed = no 
			}
			set_popularities = {
				democratic = 18
				fascism = 70
				communism = 2
				neutrality = 10
			}
		}		
		FRA = {
			set_cosmetic_tag = FRA_FREE
			clr_country_flag = no_naval_base
			remove_ideas = FRA_protected_by_the_maginot_line
			set_country_flag = free_france
			set_capital = 539
		    destroy_ships = {
				type = ship_hull_light
				count = all
			}
			destroy_ships = {
				type = ship_hull_cruiser
				count = all
			}
			destroy_ships = {
				type = ship_hull_heavy
				count = all
			}
			destroy_ships = {
				type = ship_hull_carrier 
				count = all
			}
			destroy_ships = {
				type = ship_hull_submarine
				count = all
			}
		}
		EFR = { 
			add_ideas = EFR_neutrality_idea
			diplomatic_relation = { country = ENG relation = non_aggression_pact }
			diplomatic_relation = { country = USA relation = non_aggression_pact }
			diplomatic_relation = { country = PHI relation = non_aggression_pact }
			diplomatic_relation = { country = FRA relation = non_aggression_pact }
			diplomatic_relation = { country = SAF relation = non_aggression_pact }
			diplomatic_relation = { country = CAN relation = non_aggression_pact }
			diplomatic_relation = { country = RAJ relation = non_aggression_pact }
			diplomatic_relation = { country = AST relation = non_aggression_pact }
			diplomatic_relation = { country = NZL relation = non_aggression_pact }
			diplomatic_relation = { country = GER relation = non_aggression_pact }
		}	
		if = { 
			limit = {Date < 1939.12.1}
			FRA={
				subtract_from_temp_variable = { resistancetemp = resistance }
				add_offsite_building = { type = industrial_complex level = resistancetemp }
				add_offsite_building = { type = arms_factory level = resistancetemp }
			}
			ITA = { 
				transfer_state = 735
				transfer_state = 32
				transfer_state = 21
				transfer_state = 1
				transfer_state = 458
				transfer_state = 665
			}
		}
		else={
			EFR = {
				transfer_state = 735
				transfer_state = 32
				transfer_state = 21
				transfer_state = 1
				transfer_state = 458
				transfer_state = 665
			}
		}
    	EFR = {
			add_ideas = extensive_conscription
			add_ideas = partial_economic_mobilisation
			
			#transfer_state = 26
			transfer_state = 22
			
			transfer_state = 25
			transfer_state = 20
			transfer_state = 33
			transfer_state = 31
							
			#Northern Africa
			transfer_state = 513
			
			transfer_state = 459
			transfer_state = 460
			transfer_state = 461
			transfer_state = 462
			
			transfer_state = 514
			transfer_state = 776
			transfer_state = 781
			transfer_state = 777
			transfer_state = 778
			transfer_state = 556
			transfer_state = 779
			transfer_state = 780
			transfer_state = 272
			transfer_state = 557
			transfer_state = 268
			transfer_state = 786
			transfer_state = 782
			transfer_state = 515
			transfer_state = 775
			transfer_state = 286
			transfer_state = 670
			transfer_state = 671		
			transfer_state = 741
			transfer_state = 728
			transfer_state = 543 #Madagascar
		}
		JAP = {
			EFR = { country_event = { id = france.1 } }
			transfer_state = 286
			transfer_state = 670
			transfer_state = 671		
			transfer_state = 741
			transfer_state = 728
		}
		FRA = {
			every_owned_state = {
				limit = {
					is_on_continent = africa
					is_controlled_by = GER
				}
				FRA= {set_state_controller = PREV}
			}
			set_capital = 539
		}
	}
}



#Capital captured by non french faction
country_event = {
	id = france.16
	title = france.16.t
	desc = france.16.d
	picture = GFX_report_event_french_resistance_02

	is_triggered_only = yes

	option = {
		name = france.16.a
		remove_ideas = FRA_victors_of_wwi
		remove_ideas = FRA_disjointed_government
		remove_ideas = FRA_protected_by_the_maginot_line
				set_country_flag = casablanca_capital

			if = {
			limit = { 461 = { is_owned_by = FRA }
			}
			set_capital = 461 
		}
	}
}
country_event = {
	id = france.20
	title = france.20.t
	desc = france.20.d
	picture = GFX_report_event_degaulle_churchill
	
	is_triggered_only = yes
	
	option = {
		custom_effect_tooltip = france.20.a_tt
		set_country_flag = france_de_gaulle
		hidden_effect =  {
			create_country_leader = {
				name = "Charles de Gaulle"
				desc = "POLITICS_CHARLES_DE_GAULLE_DESC"
				picture = "Portrait_France_Charles_De_Gaulle.dds"
				expire = "1965.1.1"
				ideology = conservatism
				traits = {
					
				}
			}
		}
		
		name = france.20.a
	}
}
# French strikes
country_event = {
	id = france.21
	title = france.21.t
	desc = france.21.d
	picture = GFX_report_event_worker_protests
	
	is_triggered_only = yes
	fire_only_once = yes

	option = {
		ai_chance = {
			base = 15
		}
		name = france.21.a
		add_timed_idea = {
			idea = FRA_factory_strikes
			days = 90
		}
		set_country_flag = france.21.a_chosen
	}
	option = {
		ai_chance = {
			base = 85
		}
		name = france.21.b
		add_timed_idea = {
			idea = FRA_matignon_agreements
			days = 365
		}
		add_popularity = {
			ideology = communism
			popularity = 0.06
		}		
	}
}
# Revoke Worker rights
country_event = {
	id = france.22
	title = france.22.t
	desc = france.22.d
	picture = GFX_report_event_airplane_factory

	fire_only_once = yes
	is_triggered_only = yes

	option = {
		ai_chance = {
			factor = 2
		}
		add_stability = 0.05
		name = france.22.a
	}
	option = {
		name = france.22.b
		ai_chance = { 
			factor = 10
			modifier = {
				factor = 0
				communism > 0.4
			}
		}
		remove_ideas =  FRA_matignon_agreements
		add_popularity = {
			ideology = communism
			popularity = 0.04
		}
	}	
}


news_event = {
	id = france.34
	title = france.34.title
	desc = france.34.desc
	
	picture = GFX_news_event_degaulle_churchill
	
	is_triggered_only = yes
	
	option = {
		name = france.34.a
		trigger = {
			OR = {
				tag = ENG
				tag = FRA
				is_in_faction_with = ENG
				is_subject_of = ENG
			}
		}
	}
	
	option = {
		name = france.34.b
		trigger = {
			NOT = {
				OR = {
					tag = ENG
					tag = FRA
					is_in_faction_with = ENG
					is_subject_of = ENG
				}
			}
		}
	}
}


country_event = {
   id = france.145
   title = france.145.t
   desc = france.145.d
   picture = GFX_report_event_france_startup
   fire_only_once = yes
is_triggered_only = yes
   option = {
   name = france.145.a
   }
}


country_event = {
   id = france.146
   title = france.146.t
   desc = france.146.d
   picture = GFX_report_event_fra_por_colony
   fire_only_once = yes
   is_triggered_only = yes

   option = {
   name = france.146.a
      RAJ = {
   transfer_state = 321
   321 = { add_extra_state_shared_building_slots = 1 }
   }
   }
   }
   
   country_event = {
   id = france.147
   title = france.147.t
   desc = france.147.d
   picture = GFX_report_event_fra_por_colony
   fire_only_once = yes
   is_triggered_only = yes

   option = {
   name = france.147.a
      SAF = {
   transfer_state = 544
   321 = { add_extra_state_shared_building_slots = 1 }
   }
   }
   }

   country_event = {
   id = france.148
   title = france.148.t
   desc = france.148.d
   picture = GFX_report_event_fra_por_colony
   fire_only_once = yes
   is_triggered_only = yes

   option = {
   name = france.148.a
      ENG = {
   transfer_state = 702
   }
   }
   }